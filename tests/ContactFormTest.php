<?php declare(strict_types=1);

/**
 * Main test class to validate the logic of the app
 *
 * @author Paul Brighton
 * @date June 2021
 * @since 1.0
 */

use Faker\Generator;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use Dotenv\Dotenv;
use System\Mailman;

final class ContactFormTest extends TestCase {
	/** @var string $url The full URL of the current site. USed to make HTTP requests against it */
	private static $url;

	/** @var Mailman $mailman Local instance of a mailer class */
	private static $mailman;

	/** @var Client $client Guzzle client to make HTTP requests */
	private static $client;

	/** @var Generator $faker Local instance of PHP faker @see https://fakerphp.github.io/ */
	private static $faker;

	/** @var int|string $new_id Once a new record is created successfully, keep the ID so we can delete it later */
	private static $new_id;

	/**
	 * Constructor for all tests that sets up fixtures - shared class variables
	 * This runs before the test case class’ first test
	 */
	public static function setUpBeforeClass(): void {
		// Init the .env file
		(new DotEnv(dirname(__DIR__)))->load();
		// Read the site url from .env file
		self::$url = getenv('SITE_URL');
		// Init the shared objects
		self::$mailman = new Mailman();
		self::$client  = new Client(['base_uri' => self::$url]);
		self::$faker   = Faker\Factory::create('en_US');
	}

	/**
	 * This unsets shared variables after the last test of the test case
	 *
	 */
	public static function tearDownAfterClass(): void {
		self::$mailman = self::$client = self::$faker = null;
	}

	/**
	 * Simple test to make sure the landing page loads
	 *
	 * @throws GuzzleException
	 */
	public function testLandingPage(): void {
		$response = self::$client->get('');
		$this->assertEquals(200, $response->getStatusCode());
	}

	/**
	 * Main test to make sure the contact form can be submitted
	 *
	 * @throws GuzzleException
	 */
	public function testSubmitContactForm(): void {
		// Generate a fake input
		$input    = [
			'name'    => self::$faker->name(),
			'email'   => self::$faker->safeEmail(),
			'phone'   => self::$faker->phoneNumber(),
			'message' => self::$faker->text(100),
		];
		$response = self::$client->post('inquiry', [GuzzleHttp\RequestOptions::JSON => $input]);
		$this->assertEquals(200, $response->getStatusCode());
		// Read and parse the JSON body
		$response = json_decode($response->getBody()->getContents());
		$this->assertTrue(property_exists($response, 'success'), 'Invalid response from API: ' . json_encode($response));
		$this->assertEquals(true, $response->success, 'Response contains an error: ' . $response->error);
		$this->assertNotEmpty($response->data->new_id, 'Response does not contain new inquiry ID: ' . json_encode($response));
		self::$new_id = $response->data->new_id;
	}

	/**
	 * Simple test to make sure the GET endpoint works
	 *
	 * @depends testSubmitContactForm
	 * @throws GuzzleException
	 */
	public function testGetInquiry(): void {
		$response = self::$client->get('inquiry/' . self::$new_id);
		$this->assertEquals(200, $response->getStatusCode());
		$response = json_decode($response->getBody()->getContents());
		$this->assertTrue(property_exists($response, 'success'), 'Invalid response from API: ' . json_encode($response));
		$this->assertEquals(true, $response->success, 'Response contains an error: ' . $response->error);
		$this->assertTrue(property_exists($response, 'data'), 'Response does not contain data: ' . json_encode($response));
		$this->assertTrue(property_exists($response->data, 'id'), 'Response does not contain ID: ' . json_encode($response));
		$this->assertEquals(self::$new_id, $response->data->id, 'Response ID does not match: ' . json_encode($response));
	}

	/**
	 * Simple test to make sure the PUT endpoint works
	 *
	 * @depends testSubmitContactForm
	 * @throws GuzzleException
	 */
	public function testUpdateInquiry(): void {
		// Generate a fake input
		$input    = [
			'name'    => self::$faker->name(),
			'email'   => self::$faker->safeEmail(),
			'phone'   => self::$faker->phoneNumber(),
			'message' => self::$faker->text(100),
		];
		$response = self::$client->put('inquiry/' . self::$new_id, [GuzzleHttp\RequestOptions::JSON => $input]);
		$this->assertEquals(200, $response->getStatusCode());
		$response = json_decode($response->getBody()->getContents());
		$this->assertTrue(property_exists($response, 'success'), 'Invalid response from API: ' . json_encode($response));
		$this->assertEquals(true, $response->success, 'Response contains an error: ' . $response->error);
	}

	/**
	 * Simple test to make sure the DELETE endpoint works
	 *
	 * @depends testSubmitContactForm
	 * @throws GuzzleException
	 */
	public function testDeleteInquiry(): void {
		$response = self::$client->delete('inquiry/' . self::$new_id);
		$this->assertEquals(200, $response->getStatusCode());
		$response = json_decode($response->getBody()->getContents());
		$this->assertTrue(property_exists($response, 'success'), 'Invalid response from API: ' . json_encode($response));
		$this->assertEquals(true, $response->success, 'Response contains an error: ' . $response->error);
	}

	/**
	 * Final test to make sure mailman works
	 *
	 */
	public function testMailman(): void {
		// Generate fake email
		$from_name  = self::$faker->name();
		$from_email = self::$faker->safeEmail();
		$message    = self::$faker->text(100);
		$response   = self::$mailman->send(['Guy Smiley' => 'guy-smiley@example.com'], $message, $from_name, $from_email);
		$this->assertEquals(true, $response, 'Response contains an error: ' . self::$mailman->get_error());
	}
}
