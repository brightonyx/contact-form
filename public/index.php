<?php
/**
 * Bootstrap for the entire project
 *
 * @author Paul Brighton
 * @date June 2021
 * @since 1.0
 *
 */

// Make sure the composer libraries are included
require dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use Controllers\InquiryController;
use Dotenv\Dotenv;
use System\DB;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

// Init the .env file support
(new DotEnv(dirname(__DIR__)))->load();

// Register the error handler
$whoops = new Run();

// Init our database driver
$db = new DB();

// Read the current URI path
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

// Send default headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3600');

// If this is root, render a landing page
if ($uri === '/') {
	// Set the error reporting handler to HTML
	$whoops->pushHandler(new PrettyPageHandler());
	$whoops->register();
	header('Content-Type: text/html; charset=UTF-8');
	readfile('content.html');
} else {
	// Otherwise proceed as JSON REST listener
	header('Content-Type: application/json; charset=UTF-8');
	header('Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE');
	header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

	// Set the error reporting handler to JSON
	$whoops->pushHandler(new JsonResponseHandler());
	$whoops->register();

	$uri = explode('/', $uri);
	// Only allow /inquiry resource
	if ($uri[1] !== 'inquiry') {
		header('HTTP/1.1 404 Not Found');
		exit();
	}

	// Get the HTTP method to determine the action
	$method = $_SERVER['REQUEST_METHOD'];

	// Pass the request method and the inquiry ID to the InquiryController and process the HTTP request
	$controller = new InquiryController($db);
	$controller->process_request($method, $uri);
}
