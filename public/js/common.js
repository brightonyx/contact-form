window.addEventListener('load', function() {
	// Wrapper for making AJAX calls
	(function ($) {
		$.fn.post = function (endpoint, callback) {
			// Allow only the forms to use this function
			if (this.is('form')) {
				// Process the API request
				$.ajax({
					url: endpoint,
					method: 'POST',
					contentType: 'application/json',
					data: $(this).serializeJSON(),
					fail: function () {
						showError();
					},
					error: function (xhr, textStatus, errorThrown) {
						// Show the server error. Fallback to the default AJAX exception
						let error = errorThrown;
						if (xhr.responseJSON && 'error' in xhr.responseJSON) {
							error = 'message' in xhr.responseJSON.error ? xhr.responseJSON.error.message : xhr.responseJSON.error;
						}
						showError(error);
					},
					success: function (response) {
						if (response) {
							if (response.success === true) {
								// Run the callback if defined
								if (callback) {
									callback(response);
								}
								if (response.message) {
									showSuccess(response.message);
								}
							} else {
								renderError(response.error);
							}
						} else {
							showError('No data received from the server...');
						}
					}
				});
			} else {
				showError('post() method can only be applied to forms');
			}
			return this;
		};
	})(jQuery);

	// Creates a proper JSON object from form data
	(function ($) {
		$.fn.serializeJSON = function () {
			let jsonObject = {};
			jQuery.map($(this).serializeArray(), function (n, i) {
				jsonObject[n['name']] = n['value'];
			});
			return JSON.stringify(jsonObject);
		};
	})(jQuery);

	// Get the alert box and set is as a global variable
	let alert = $('#alert');

	/**
	 * Shows an error in the 'alert' container
	 *
	 * @param message
	 */
	function showError(message = 'Form submit error. Please try again') {
		// Remove any success classes and add an error class
		alert.removeClass('alert-success').addClass('alert-danger');
		// Show the text and apply the pre-defined transitions
		updateAlertContainer(message);
	}

	/**
	 * Shows a success message in the 'alert' container
	 *
	 * @param message
	 */
	function showSuccess(message = 'Form submitted successfully') {
		// Remove any error classes and add a success class
		alert.removeClass('alert-danger').addClass('alert-success');
		// Show the text and apply the pre-defined transitions
		updateAlertContainer(message);
	}

	/**
	 * Internal method that fades in and out an actual element
	 *
	 * @param message
	 */
	function updateAlertContainer(message) {
		// Inject the message text and trigger a transition - show slow
		alert.html(message).fadeIn('slow');
		// Remove the message after 5 seconds
		setTimeout(function () {
			// Set the transition to 3 seconds
			alert.fadeOut(3000);
		}, 5000);
	}
}, false);