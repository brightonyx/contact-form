<?php
/**
 * Simple input validator to sanitize user data
 *
 * @author Paul Brighton
 * @date June 2021
 * @since 1.0
 */

namespace Validators;

use System\Util;

class Input {

	/**
	 * Checks if the value is an integer
	 *
	 * @param mixed $value
	 * @param string $error
	 * @return false|mixed
	 */
	public static function int($value, string $error = 'Invalid integer') {
		$value = filter_var($value, FILTER_VALIDATE_INT);
		if ($value === false) {
			Util::json_error($error);
		}
		return $value;
	}

	/**
	 * Checks if the value is a string
	 *
	 * @param mixed $value
	 * @param string $error
	 * @return string
	 */
	public static function str($value, string $error = 'Invalid string'): string {
		if (!is_string($value)) {
			Util::json_error($error);
		}
		return trim(htmlspecialchars($value));
	}

	/**
	 * Checks if the value is an email
	 *
	 * @param mixed $value
	 * @param string $error
	 * @return false|mixed
	 */
	public static function email($value, string $error = 'Invalid email') {
		$value = filter_var($value, FILTER_VALIDATE_EMAIL);
		if ($value === false) {
			Util::json_error($error);
		}
		return $value;
	}

	/**
	 * Formats an optional phone number
	 *
	 * @param mixed $value
	 * @return string
	 */
	public static function phone($value = null): string {
		// Because phone is optional, skip the formatting on null
		if (!$value) {
			return $value;
		}
		// First, sanitize the value by removing every characters but numbers (also remove +1 extension. Sometimes added by php faker)
		$value = preg_replace('/\D/', '', str_replace(['+1', '1-'], '', $value));
		// Then, format the phone number to a North American standard
		return preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '$1-$2-$3', $value);
	}
}