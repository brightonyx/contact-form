<?php
/**
 * Primitive wrapper for establishing a database connection and using simple methods to persist data
 *
 * @author Paul Brighton
 * @date June 2021
 * @since 1.0
 *
 */

namespace System;

use PDO;

class DB {

	/** @var PDO $db Local instance of a database driver */
	private $db;

	/** @var string TABLE_NAME defines a table name used for storing contact information */
	public const TABLE_NAME = 'site_inquires';

	/**
	 * Class constructor and the main DB driver instantiator
	 * Tries to connect to a database using the credentials specified in the .env file
	 * Also watches for the required table and creates it if necessary
	 *
	 */
	public function __construct() {
		$host     = getenv('DB_HOST');
		$port     = getenv('DB_PORT');
		$database = getenv('DB_DATABASE');
		$username = getenv('DB_USERNAME');
		$password = getenv('DB_PASSWORD');
		$options  = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$this->db = new PDO("mysql:host=$host;port=$port;charset=utf8mb4;dbname=$database", $username, $password, $options);
		// Make sure the required table exists
		if (!$this->table_exists()) {
			// If it doesn't, create it
			$this->create();
		}
	}

	/**
	 * Retrieves all site inquires
	 *
	 * @return array
	 */
	public function find(): array {
		$statement = 'SELECT * FROM ' . self::TABLE_NAME;
		$statement = $this->db->query($statement);
		return $statement->fetchAll();
	}

	/**
	 * Gets a specific inquiry by ID
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function find_by_id(int $id) {
		$statement = 'SELECT * FROM ' . self::TABLE_NAME . ' WHERE id = ?';
		$statement = $this->db->prepare($statement);
		$statement->execute([$id]);
		return $statement->fetch();
	}

	/**
	 * Persists sanitized data
	 *
	 * @param array $input
	 * @return int Returns record count
	 */
	public function insert(array $input): int {
		$ip        = Util::get_client_ip();
		$statement = 'INSERT INTO ' . self::TABLE_NAME . " (full_name, email, phone, message, ip) VALUES (:name, :email, :phone, :message, '$ip')";
		$statement = $this->db->prepare($statement);
		$statement->execute($input);
		return $this->db->lastInsertId();
	}

	/**
	 * Updates a record with the new data
	 * The input array must contain an ID
	 *
	 * @param array $input
	 * @return int Returns record count
	 */
	public function update(array $input): int {
		$statement = 'UPDATE ' . self::TABLE_NAME . " SET full_name = :name, email = :email, phone = :phone, message = :message WHERE id = :id";
		$statement = $this->db->prepare($statement);
		$statement->execute($input);
		return $statement->rowCount();
	}

	/**
	 * Deletes a record given an ID
	 *
	 * @param int $id
	 * @return int Returns record count
	 */
	public function delete(int $id) {
		$statement = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE id = ?';
		$statement = $this->db->prepare($statement);
		$statement->execute([$id]);
		return $statement->rowCount();
	}


	/**
	 * Checks to see if a required table exists in the database
	 *
	 * @return bool
	 */
	private function table_exists(): bool {
		$statement = $this->db->query("SHOW TABLES LIKE '" . self::TABLE_NAME . "'");
		return (bool)count($statement->fetchAll());
	}

	/**
	 * Creates a required table
	 *
	 */
	private function create(): void {
		$table_name = self::TABLE_NAME;
		$query      = <<<MYSQL
CREATE TABLE IF NOT EXISTS $table_name (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    phone VARCHAR(12),
    message TEXT,
    ip VARCHAR(15),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=INNODB;
MYSQL;
		$this->db->exec($query);
	}
}