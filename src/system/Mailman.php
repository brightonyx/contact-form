<?php
/**
 * Mailman is a mailer service to deliver emails using a given SMTP credentials
 *
 * @author Paul Brighton
 * @date June 2021
 * @since 1.0
 */

namespace System;

use PHPMailer\PHPMailer\PHPMailer;
use Throwable;

class Mailman {

	/** @var PHPMailer $smtp Local instance of a mailer class */
	private $smtp;

	/**
	 * Class constructor and the main mail instantiator
	 *
	 */
	public function __construct() {
		$host       = getenv('MAIL_HOST');
		$port       = getenv('MAIL_PORT');
		$from       = getenv('MAIL_FROM_ADDRESS');
		$username   = getenv('MAIL_USERNAME');
		$password   = getenv('MAIL_PASSWORD');
		$encryption = getenv('MAIL_ENCRYPTION');

		// Init the php mailer and pass the SMTP credentials
		$this->smtp = new PHPMailer();
		$this->smtp->isSMTP();
		$this->smtp->Host       = $host;
		$this->smtp->SMTPAuth   = true;
		$this->smtp->SMTPSecure = $encryption;
		$this->smtp->Username   = $username;
		$this->smtp->Password   = $password;
		$this->smtp->Port       = $port;

		try {
			$this->smtp->setFrom($from, 'Mailtrap');
			$this->smtp->addReplyTo($from, 'Mailtrap');
			/*
			 Also, enable SMTP debugging using one of the levels
			    level 0 = turns the debugging off
			    level 1 = client; shows messages sent by the client only
			    level 2  = client and server; adds server messages (recommended)
			    level 3 = client, server, and connection; recommended for exploring STARTTLS failures
			    level 4 = low-level information
			 */
			$this->smtp->SMTPDebug = 0;
		} catch (Throwable $e) {
			Util::json_error($e->getMessage());
		}
	}

	/**
	 * Dispatches an email message
	 *
	 * @param array $to Array of recipients either with names or just email addresses: ['John' => 'john@email.com', 'Bob' => 'bob@email.com'] OR ['john@email.com', 'bob@email.com']
	 * @param string $body HTML content of an email message
	 * @param string|null $from_name Define who this message is from
	 * @param string|null $from_email Define who this message is from
	 * @param string|null $subject Email subject
	 * @return bool
	 */
	public function send(array $to, string $body, string $from_name = 'Mailtrap', string $from_email = null, string $subject = 'Contact Form'): bool {
		try {
			foreach ($to as $name => $address) {
				// Check if the name is provided
				if (is_int($name)) {
					$name = null;
				}
				$this->smtp->addAddress($address, $name);
			}
			$this->smtp->Body    = strip_tags($body);
			$this->smtp->Subject = $subject;

			// If $from was given, use that instead of a default address
			if ($from_email) {
				$this->smtp->setFrom($from_email, $from_name);
				$this->smtp->addReplyTo($from_email, $from_name);
			}

			// Try to send the email
			if (!$this->smtp->send()) {
				return false;
			}
			return true;
		} catch (Throwable $e) {
			$this->smtp->ErrorInfo = $e->getMessage();
		}

		// Got here somehow, clearly an error
		return false;
	}

	/**
	 * Helper method to fetch the last error message from the SMTP server
	 *
	 * @return string
	 */
	public function get_error(): string {
		if ($this->smtp and $error = $this->smtp->ErrorInfo) {
			return $error;
		}
		return '';
	}
}