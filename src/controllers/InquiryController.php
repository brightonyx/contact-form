<?php
/**
 * Inquiry controller that routes the request
 *
 * @author Paul Brighton
 * @date June 2021
 * @since 1.0
 */

namespace Controllers;

use System\DB;
use System\Mailman;
use System\Util;
use Validators\Input;

class InquiryController {

	/** @var DB $db Local reference to the DB object */
	private $db;

	/** @var Mailman $mailman Local reference to the Mailman object */
	private $mailman;

	/**
	 * InquiryController constructor.
	 *
	 * @param DB $db
	 */
	public function __construct(DB $db) {
		$this->db = $db;
		$this->mailman = new Mailman();
	}

	/**
	 * Reads the HTTP request method and performs a corresponding model action
	 * 
	 * @param string $request_method
	 * @param array $uri
	 */
	public function process_request(string $request_method, array $uri): void {
		// See if we have an ID for retrieving details about a specific inquiry
		$inquiry_id = Util::arval($uri, 2);
		// Get user input from a JSON payload
		$user_data = json_decode(file_get_contents('php://input'), true);
		switch ($request_method) {
			case 'GET':
				if ($inquiry_id) {
					$this->get_inquiry_by_id((int)$inquiry_id);
				} else {
					$this->get_all_inquiries();
				}
				break;
			case 'POST':
				$this->create_new_inquiry($user_data);
				break;
			case 'PUT':
				$this->update_inquiry($inquiry_id, $user_data);
				break;
			case 'DELETE':
				$this->delete_inquiry($inquiry_id);
				break;
			default:
				Util::json_error('Invalid request method');
				break;
		}

	}

	/**
	 * Returns all inquires from a database
	 * This is not production ready code of course, it is missing security, pagination, etc
	 *
	 */
	private function get_all_inquiries(): void {
		$results = $this->db->find();
		if (!$results) {
			Util::json_error('No results found');
		}
		Util::json_data($results);
	}

	/**
	 * Similarly, tries to find an individual inquiry
	 *
	 * @param int $id
	 */
	private function get_inquiry_by_id(int $id): void {
		$results = $this->db->find_by_id($id);
		if (!$results) {
			Util::json_error('No results found matching this ID');
		}
		Util::json_data($results);
	}

	/**
	 * Inserts a new inquiry record by ready the POST array
	 *
	 * @param array $user_data
	 */
	private function create_new_inquiry(array $user_data = []): void {
		// Validate and clean the input
		$input = $this->validate_input($user_data);
		// If we have errors, exit with a message
		if (!$new_id = $this->db->insert($input)) {
			Util::json_error('Error saving your form. Please try again later');
		}
		// Else, send an email and exit with a success message
		if (!$this->mailman->send(['Guy Smiley' => 'guy-smiley@example.com'], $user_data['message'], $input['name'], $input['email'])) {
			Util::json_error('Message could not be sent: ' . $this->mailman->get_error());
		}
		Util::json_data(['new_id' => $new_id], 'Contact form submitted successfully');
	}

	/**
	 * Updates an existing record
	 *
	 * @param int $id
	 * @param array $user_data
	 */
	private function update_inquiry(int $id, array $user_data = []): void {
		// First, see if we can find the record by ID
		if (!$this->db->find_by_id($id)) {
			Util::json_error('No results found matching this ID');
		}
		// Validate and clean the input
		$input = $this->validate_input($user_data);
		// Add the ID to the input
		$input['id'] = $id;
		// If we have errors, exit with a message
		if (!$this->db->update($input)) {
			Util::json_error('Error saving your form. Please try again later');
		}
		Util::json_success('Contact form was updated successfully');
	}

	/**
	 * Deletes an existing record
	 * Similar to most methods above: this is not production ready as this is a destructive function
	 *
	 * @param int $id
	 */
	private function delete_inquiry(int $id): void {
		if (!$this->db->find_by_id($id)) {
			Util::json_error('No results found');
		}
		// If we have errors, exit with a message
		if (!$this->db->delete($id)) {
			Util::json_error('Error deleting an inquiry. Please try again later');
		}
		Util::json_success('Contact inquiry was deleted successfully');
	}

	/**
	 * Helper method to check for required fields and sanitize the input in one shot
	 *
	 * @param array $post
	 * @return array
	 */
	private function validate_input(array $post): array {
		// Init a clean input array
		$input = [];
		// Make sure the required fields are present
		if (!$name = Util::arval($post, 'name')) {
			Util::json_error('Full name is required', 422);
		} elseif (!$email = Util::arval($post, 'email')) {
			Util::json_error('Email address is required', 422);
		} elseif (!$message = Util::arval($post, 'message')) {
			Util::json_error('Message is required', 422);
		}
		// Next, sanitize the input
		$input['name']    = Input::str($name);
		$input['email']   = Input::email($email);
		$input['message'] = Input::str($message);
		$input['phone']   = Input::phone(Util::arval($post, 'phone'));
		return $input;
	}
}