# Dealer Inspire PHP Code Challenge

Original problem instructions can be found [here](https://bitbucket.org/dealerinspire/php-contact-form/src/master/README.md). This readme focuses on the solution.

## Foundation

My approach was to use minimal dependencies and external libraries. Therefore, I did not use a framework for either front- or backend. The only composer dependencies are:

- `vlucas/phpdotenv` - used for reading .env files
- `phpmailer/phpmailer` - used for sending out emails given SMTP credentials
- `phpunit/phpunit` - used for creating unit tests 
- `guzzlehttp/guzzle` - used by phpunit for making API requests
- `fakerphp/faker` - used by phpunit for generating test data
- `roave/security-advisories` - used to check for installed dependencies with known security vulnerabilities
- `filp/whoops` - used for generating human-readable exception

This app features a default route for the landing page (`/`) as well as one single RESTful endpoint (`/inquiry`). Multiple contact form records are names inquires, hence the endpoint name `inquiry`

## Building the project locally

Configure your web server to point to `/public` folder as a directory root and serve index.php. Next, run 
```
composer install
``` 
and configure your DB and SMTP settings in the `.env` file. 

## The structure

Because there is no framework, the code structure is fairly simple and straight-forward. 

- `/public/index.php` is the main bootstrap that deals with autoloading, initializing services, routing, and error handling
- `/src/controllers/InquiryController.php` is called to process the HTTP request
- `/src/system` is a collection of system classes and helpers
- `/tests` phpunit test class containing multiple methods to test the REST functionality as well as the mailer

## Accessing the app

When accessing the root path (for example http://localhost), the default HTML page will be rendered with the contact form embedded. Database connection will be established and a missing table will be created.

All backend requests and responses are done via JSON only. I added a jQuery prototype for submitting forms which can be invoked with `$('#form').post({endpoint});`

There are two places where input validation happens: front end via HTML5 _pattern_ directive, and backend via a _Input_ validator class

Once a first inquiry is created, a list of all records in the database can be viewed at http://localhost/inquiry

## Testing
### phpunit:

```
./vendor/bin/phpunit tests
```
### REST:

This app is RESTful and accepts the following calls:
```
POST /inquiry ::: {"name":"John Doe","email":"john@doe.com","message":"Site is broken"}
PUT /inquiry/$id ::: {"name":"John Doe","email":"john@doe.com","message":"Site is fixed"}
GET /inquiry/$id
DELETE /inquiry/$id
```

## Responses

All backend calls respond with a consistent `Response` object. Sample output is provided:
```
{
    "success": true,
    "error": null,
    "message": "Contact form submitted successfully",
    "data": {
        "new_id": 1
    },
    "requested_at": "2021-06-16 03:31:02"
}
```
OR
```
{
    "success": true,
    "error": null,
    "message": null,
    "data": [
        {
            "id": 1,
            "full_name": "Mathias d'Arras",
            "email": "test@mailnator.com",
            "phone": "123-456-7894",
            "message": "iubniew finbiuew nbiuf niewju nij ie uibn923hf938 2ndf 32 ndfoi n2o3i ndo2",
            "ip": "127.0.0.1",
            "created_at": "2021-06-15 23:31:02"
        }
    ],
    "requested_at": "2021-06-16 03:32:22"
}
```